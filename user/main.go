package main

import (
	"log"
	"net"
)

var Connections = []net.Conn{}

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		log.Fatal("tcp server listener error:", err)
	}

	for {
		conn1, err := listener.Accept()
		if err != nil {
			log.Fatal("tcp server accept error", err)
		}
		go handleConnection1(conn1)

		conn2, err := listener.Accept()
		if err != nil {
			log.Fatal("tcp server accept error", err)
		}
		go handleConnection2(conn2)
	}
}

func handleConnection1(conn net.Conn) {
	for {
		conn.Read()
	}
}

func handleConnection1(conn net.Conn) {

	for _, v := range Connections {
		v.Write([]byte("hello"))
	}

}
