package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"os"
)

var currentPacket uint64 = 0
var cachedPacket = make(map[uint64]bool)

func main() {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Please provide host:port.")
		return
	}

	CONNECT := arguments[1]
	c, err := net.Dial("tcp", CONNECT)
	if err != nil {
		fmt.Println(err)
		return
	}
	//buf := []byte{}

	for {
		rdr := bufio.NewReader(c)
		data := make([]byte, 0, 4*1024)
		vdata := make([]byte, 0, 4*1024)
		for {
			n, err := io.ReadFull(rdr, data[:23])
			data = data[:n]
			if err != nil {
				if err == io.EOF {
					break
				}
				return
			}
			//if string(data[:11]) == "FRAMEzato01" {
			//	fmt.Println(string(data[:11]), binary.BigEndian.Uint64(data[11:19]), binary.BigEndian.Uint32(data[19:23]))
			io.ReadFull(rdr, vdata[:binary.BigEndian.Uint32(data[19:23])])
			//}

			handlePacket(data)
		}
	}
}

func handlePacket(data []byte) {
	if string(data[:11]) == "FRAMEzato01" { // channel 1
		if currentPacket == binary.BigEndian.Uint64(data[11:19]) { // this packet is in sequence --> send to user
			fmt.Println("here 1", string(data[:11]), binary.BigEndian.Uint64(data[11:19]), binary.BigEndian.Uint32(data[19:23]))
			currentPacket++
		} else { // send to cache
			cachedPacket[binary.BigEndian.Uint64(data[11:19])] = true
		}

		// check if next packages are in cache
		for {
			//fmt.Println("here", string(data[:11]), binary.BigEndian.Uint64(data[11:19]), binary.BigEndian.Uint32(data[19:23]))
			if _, ok := cachedPacket[currentPacket]; ok {
				fmt.Println("here 2", string(data[:11]), currentPacket, binary.BigEndian.Uint32(data[19:23]))
				delete(cachedPacket, currentPacket)
				currentPacket++
			} else {
				break
			}
			//fmt.Println("in for loop")
		}
	}
	//fmt.Println(cachedPacket, currentPacket)
}
